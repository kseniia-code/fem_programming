#include "calculations.h"
#include <QDebug>
#include <core/gmrandom>


using namespace GMlib;


Calculations::Calculations()
{

}

bool Calculations::pointOk(GMlib::Point<float,2> pt, GMlib::ArrayLX<GMlib::TSVertex<float> > vt, float min_dist) {

    for (int i = 0; i < vt.getSize(); i++ ){
        GMlib::Vector<float,2> d;
        d = pt - vt[i].getParameter();
        if ( d.getLength() < min_dist)
            return false;
    }
    return true;
}

void Calculations::CalculateRandom(){

    int k = 40; //number of points in the boundary
    float r = 5.0f; //radius
    float r_inner;
    int n; //number of internal points
    int t; //number of triangels
    float min_dist; //minimal distance between points
    //int l = 0;
    float a;

    //compute the number of the regular triangles t

    a = (M_2PI*r)/k; // approximate length of 1 side of the triangle
    float b = M_PI * r * r; // area of the circle
    float c = std::sqrt(3)/4 * a * a; // area of triangle
    t = int(b/c);

    //t = (k^2)/(std::sqrt(3)*M_2PI);

    std::cout << "t :" << t << std::endl;

    //compute the number of internal points n, using k and t

    n = 1.0f + int(float(t - k)/2.0f + 0.5f);
    //n = 1.0f + int(float(t - k)/2.0f);

    std::cout << "n :" << n << std::endl;

    //GMlib::Point<float,2> _p(0.0, 0.0);
    vt.setSize(n+k); // n+k
 //   vt[0].setPos(_p);

    r_inner = std::sqrt(r*r - (a/2.0f)*(a/2.0f));

    std::cout << "r_inner :" << r_inner << std::endl;

    min_dist = 2*std::sqrt(b/(std::sqrt(3)*t))/1.7;

    //make the boundary

    for (int i = 0; i < k; i++ ){

        Angle a = i*M_2PI/k;
        GMlib::SqMatrix<float,2> mt(a, GMlib::Vector<float,2> (1,0),GMlib::Vector<float,2> (0,1));
        GMlib::Point<float,2> p = mt * GMlib::Vector<float,2>(r,0);
        vt[i].setPos(p);

    }

    GMlib::Random<float> rand(-r , r);

    GMlib::Point<float,2> pt;

    for (int i = 0 ; i < n; i++){

        do{
            do{
                pt = GMlib::Point<float, 2> (rand.get(), rand.get());

            } while (pt.getLength() > r_inner); //r_inner

        } while (!pointOk (pt, vt, min_dist));

        //vt += GMlib::TSVertex<float>(pt);
        vt[k+i].setPos(pt);

    }

    qDebug() << "end of loop";
    insertAlways(vt);
    qDebug() << "Vertex" << vt.getSize();
}


void Calculations::CalculateRegular(){

    GMlib::Point<float,2> _center(0.0, 0.0); //center position

    int n = 8; //number of points on first circle
    int m = 6; //nr_circles
    int k = 0;
    float r = 5.0f; //radius

    VertexArray.setSize(1+n*(m*(m+1))/2);
    VertexArray[0].setPos(_center);

    for (int j = 0; j < m; j++ ){

        for (int i = 0; i < n*(j+1); i++ ){

            Angle a = i*M_2PI/(n*(j+1));
            GMlib::SqMatrix<float,2> mt(a, GMlib::Vector<float,2> (1,0),GMlib::Vector<float,2> (0,1));
            GMlib::Vector<float,2> p(((j+1)*r)/m,0); //new position of vector
            VertexArray[k++].setPos(mt*p);
        }
    }

    insertAlways(VertexArray);

    qDebug() << "Vertex" << VertexArray.getSize();

}

void Calculations::makeNodes(){

    //make nodes - exept boundary - function
    //Nodes.setSize(this->size());
    for (int i = 0; i < this->getSize(); i++ )
        if (!(*this)[i].boundary())
            Nodes += Node((*this)[i]);
    //Nodes.insertAlways(Node(VertexArray[i]));
    qDebug() << "Nodes" << Nodes.getSize();

    //    for(int i =0 ; i < Nodes.size(); i++)
    //    qDebug() << Nodes[i].isNeighbor(Nodes[i]);

}

void Calculations::makeStiffnessM(){

    A.setDim(Nodes.getSize(), Nodes.getSize());

    //set o to all elements
    for(int i =0 ; i < Nodes.size(); i++)
        for (int j = 0 ; j < Nodes.size(); j++ )
            A[i][j] = 0;

    for(int i =0 ; i < Nodes.size(); i++){
        for (int j = i+1 ; j < Nodes.size(); j++ ){

            e = Nodes[i].isNeighbor(Nodes[j]);

            //if(Nodes[i].isNeighbor(Nodes[j]) && (e != NULL)){

            if(e != nullptr){

                Vector<Vector<float,2>,3> vector = findVectors(e);

                //vector definitions
                GMlib::Vector<float,2> d;
                GMlib::Vector<float,2> a1;
                GMlib::Vector<float,2> a2;

                d = vector[0];
                a1 = vector[1];
                a2 = vector[2];

                //Properties of diagonal d

                float dd = 1.0f /(d*d);

                //Triangle (T1)
                float area1 = std::abs(d^a1);
                float dh1 = dd*(a1*d);
                float h1 = dd*area1*area1;

                //Triangle (T2)
                float area2 = std::abs(d^a2);
                float dh2 = dd*(a2*d);
                float h2 = dd*area2*area2;

                A[i][j] = A[j][i] = ((dh1*(1 - dh1))/h1 - dd)*(area1/2.0f) + ((dh2*(1 - dh2))/h2 - dd)*(area2/2.0f);

            }

            else A[i][j] = A[j][i] = 0;

        }

        Array<TSTriangle<float>*>tr = Nodes[i].getVertex()->getTriangles();
        for (int k = 0 ; k < tr.size(); k++ ){
            Vector<Vector<float,2>,3> d = findVectors(tr[k], &Nodes[i]);
            A[i][i] += (d[2]*d[2])/(2*(std::abs(d[0]^d[1])));

        }

        //std::cout<<A[i][i]<<std::endl;

    }
    //end of loop for A[i][j]

    //Computing the load vector

    b.setDim(Nodes.size());
    for (int i = 0; i<Nodes.size(); i++){
        Array<TSTriangle<float> *> tr = Nodes[i].getVertex()->getTriangles();
        b[i] = tr[0]->getArea2D()/3;
        for (int j = 1; j < tr.size(); j++)
            b[i] += tr[j]->getArea2D()/3;
    }

    _A = A.invert();
    update = 0.0;
    up = true;

}

GMlib::Vector<GMlib::Vector<float,2>, 3> Calculations::findVectors(GMlib::TSEdge<float>* e){

    Vector<Vector<float,2>, 3> d;
    Array<TSTriangle<float> *> tr = e->getTriangle();
    Array<TSVertex<float> *> v1 = tr[0]->getVertices();
    Array<TSVertex<float> *> v2 = tr[1]->getVertices();

    Point<float,2> p0, p1, p2, p3;
    p0 = e->getFirstVertex()->getParameter();
    p1 = e->getLastVertex()->getParameter();
    for (int i = 0; i < 3; i++){
        if(v1[i] != e->getFirstVertex() && v1[i] != e->getLastVertex()) p2 = v1[i]->getParameter();
        if(v2[i] != e->getFirstVertex() && v2[i] != e->getLastVertex()) p3 = v2[i]->getParameter();
    }

    d[0] = p1-p0;
    d[1] = p2-p0;
    d[2] = p3-p0;
    return d;
}

Vector<Vector<float,2>, 3> Calculations::findVectors(TSTriangle<float>* tr, Node *node){
    Vector<Vector<float,2>, 3> d;
    Point<float,2> p0, p1, p2;
    Array<TSVertex<float> *> v = tr->getVertices();
    if (node->isVertex(*v[1])){
        std::swap(v[0], v[1]);
        std::swap(v[1], v[2]);
    }
    if (node->isVertex(*v[2])){
        std::swap(v[0], v[2]);
        std::swap(v[1], v[2]);
    }
    p0 = v[0]->getParameter();
    p1 = v[1]->getParameter();
    p2 = v[2]->getParameter();
    d[0] = p1-p0;
    d[1] = p2-p0;
    d[2] = p2-p1;
    return d;
}

void Calculations::localSimulate(double dt) {

    GMlib::DVector<float> x;

    if(up)
        update += dt;
    else
        update -= dt;
    if(update > 0.5)
        up = false;
    if(update < -0.5)
        up = true;

    for(int i =0 ; i < Nodes.size(); i++){
        //Compute values for new vertices
        x = _A*(update*b);
        Nodes[i].getVertex()->setZ(x[i]);
    }
    this->replot();
}



