#ifndef NODE_H
#define NODE_H

#include <trianglesystem/gmtrianglesystem.h>

class Node{

private:

    GMlib::TSVertex<float>* _v;



public:
  //using TSVertex::TSVertex;

    GMlib::TSVertex<float>* getVertex(){
        return _v;
    }

    Node(GMlib::TSVertex<float>& v) {

    _v = &v;

    }

    Node() {

    _v = nullptr;

    }

    void setPosition ( GMlib::Point<float,2> p) {
        _v->setPos(p);
    }

    ~Node() {}

    GMlib::TSEdge<float>* isNeighbor(Node &a){
        auto s = _v->getEdges();
            for(int i = 0 ; i < s.getSize() ; i++)
                  if(a.isVertex(*s[i]->getOtherVertex(*_v)))
                      return s[i];
            return nullptr;

    }

    bool isVertex(GMlib::TSVertex<float> &vertex)
    {
            return &vertex == _v;
    }

}; // END class Node

#endif // NODE_H

