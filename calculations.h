#ifndef CALCULATIONS_H
#define CALCULATIONS_H

#include <core/gmarray>
#include <core/gmmatrix>
#include "node.h"

class Calculations: public GMlib::TriangleFacets<float> {

public:

    Calculations();
    ~Calculations() {}
    void CalculateRegular();
    void CalculateRandom();
    void makeNodes();
    void makeStiffnessM();
    void makeLoadVector();
    void localSimulate(double dt);
    GMlib::Vector<GMlib::Vector<float,2>, 3> findVectors(GMlib::TSEdge<float>* e);
    GMlib::Vector<GMlib::Vector<float,2>, 3> findVectors(GMlib::TSTriangle<float> * tr, Node *node);
    bool pointOk(GMlib::Point<float,2> pt,  GMlib::ArrayLX<GMlib::TSVertex<float>> vt, float min_dist);


private:

    GMlib::Point<float,2> pos;
    ArrayLX<Node> Nodes;
    ArrayLX<GMlib::TSVertex<float>> VertexArray;
    ArrayLX <GMlib::TSVertex<float>> vt;
    GMlib::DMatrix<float> A, _A;
    GMlib::DVector<float> b;
    GMlib::TSEdge<float>* e;
    double update;
    bool up;
};



#endif // CALCULATIONS_H

